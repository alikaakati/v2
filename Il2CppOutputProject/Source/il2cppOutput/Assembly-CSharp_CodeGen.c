﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Agent::Start()
extern void Agent_Start_mFB613B0E96F19F4A125D553149A9FA3B3E165475 ();
// 0x00000002 System.Void Agent::Update()
extern void Agent_Update_mEA15C25B2619D350AF8F3356C82600E3A9F51B5F ();
// 0x00000003 System.Void Agent::OnTriggerEnter(UnityEngine.Collider)
extern void Agent_OnTriggerEnter_mE4E5200A760D0C779D28D960F0A1D0F74DD97668 ();
// 0x00000004 System.Void Agent::.ctor()
extern void Agent__ctor_mFA159E6DA69FBFDD5CE2061FA66BCEB355C48712 ();
// 0x00000005 System.Void Dialogue::.ctor()
extern void Dialogue__ctor_mEDBBF824F54F6A922A280A71EF8E4A697873108F ();
// 0x00000006 System.Void dialogueManager::Start()
extern void dialogueManager_Start_m4C13FFC514C7C9CCD891B90D3C1265283F8BC611 ();
// 0x00000007 System.Void dialogueManager::Start(Dialogue)
extern void dialogueManager_Start_m2529F09532BB93B29E9432DF82CE144BD5E2F36D ();
// 0x00000008 System.Void dialogueManager::DisplayNextSentence()
extern void dialogueManager_DisplayNextSentence_mA908BA851D59F16425BB72F11E8F188DE4BEF6AC ();
// 0x00000009 System.Void dialogueManager::EndDialogue()
extern void dialogueManager_EndDialogue_m85ECA25E3E8A58309AA6917974824F962ACE9906 ();
// 0x0000000A System.Void dialogueManager::.ctor()
extern void dialogueManager__ctor_m33C163E3B4C80302454C53FBA26F5F6DEBE6E803 ();
// 0x0000000B System.Void dialogueTrigger::Start()
extern void dialogueTrigger_Start_m72F12AB8B631FE55459F49F8CB8C0D2E38FD9522 ();
// 0x0000000C System.Void dialogueTrigger::Update()
extern void dialogueTrigger_Update_m98BEC3DFD545FEEC105C7759E1086BE64D9C8BDB ();
// 0x0000000D System.Void dialogueTrigger::.ctor()
extern void dialogueTrigger__ctor_m23F9C11D3A8DD8A597B38985643F3457B94D605F ();
// 0x0000000E System.Void followCamera::Start()
extern void followCamera_Start_m5143CD8FDDC0D28D751511C178A2C5F1E781D7BE ();
// 0x0000000F System.Void followCamera::Update()
extern void followCamera_Update_mD57C3DCF6151CEBC86232706D6D17A27F7E6921D ();
// 0x00000010 System.Void followCamera::.ctor()
extern void followCamera__ctor_mCC22E39C9130C8F043DA4828AE288C32DEE0630B ();
// 0x00000011 System.Void RobotFreeAnim::Awake()
extern void RobotFreeAnim_Awake_m41547D9AE6F2C20352FDF8ECB3A57DE4179DF5D9 ();
// 0x00000012 System.Void RobotFreeAnim::Update()
extern void RobotFreeAnim_Update_m26B41E76E16FD42316661AF3AD8ACF984E698805 ();
// 0x00000013 System.Void RobotFreeAnim::CheckKey()
extern void RobotFreeAnim_CheckKey_m7CDA03F8A5DE53AC779C90C6EFF9123F5C57B4B1 ();
// 0x00000014 System.Void RobotFreeAnim::.ctor()
extern void RobotFreeAnim__ctor_m22247C2C5C0578F83B29FEECEE4F6B9113240C79 ();
static Il2CppMethodPointer s_methodPointers[20] = 
{
	Agent_Start_mFB613B0E96F19F4A125D553149A9FA3B3E165475,
	Agent_Update_mEA15C25B2619D350AF8F3356C82600E3A9F51B5F,
	Agent_OnTriggerEnter_mE4E5200A760D0C779D28D960F0A1D0F74DD97668,
	Agent__ctor_mFA159E6DA69FBFDD5CE2061FA66BCEB355C48712,
	Dialogue__ctor_mEDBBF824F54F6A922A280A71EF8E4A697873108F,
	dialogueManager_Start_m4C13FFC514C7C9CCD891B90D3C1265283F8BC611,
	dialogueManager_Start_m2529F09532BB93B29E9432DF82CE144BD5E2F36D,
	dialogueManager_DisplayNextSentence_mA908BA851D59F16425BB72F11E8F188DE4BEF6AC,
	dialogueManager_EndDialogue_m85ECA25E3E8A58309AA6917974824F962ACE9906,
	dialogueManager__ctor_m33C163E3B4C80302454C53FBA26F5F6DEBE6E803,
	dialogueTrigger_Start_m72F12AB8B631FE55459F49F8CB8C0D2E38FD9522,
	dialogueTrigger_Update_m98BEC3DFD545FEEC105C7759E1086BE64D9C8BDB,
	dialogueTrigger__ctor_m23F9C11D3A8DD8A597B38985643F3457B94D605F,
	followCamera_Start_m5143CD8FDDC0D28D751511C178A2C5F1E781D7BE,
	followCamera_Update_mD57C3DCF6151CEBC86232706D6D17A27F7E6921D,
	followCamera__ctor_mCC22E39C9130C8F043DA4828AE288C32DEE0630B,
	RobotFreeAnim_Awake_m41547D9AE6F2C20352FDF8ECB3A57DE4179DF5D9,
	RobotFreeAnim_Update_m26B41E76E16FD42316661AF3AD8ACF984E698805,
	RobotFreeAnim_CheckKey_m7CDA03F8A5DE53AC779C90C6EFF9123F5C57B4B1,
	RobotFreeAnim__ctor_m22247C2C5C0578F83B29FEECEE4F6B9113240C79,
};
static const int32_t s_InvokerIndices[20] = 
{
	13,
	13,
	4,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	20,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
